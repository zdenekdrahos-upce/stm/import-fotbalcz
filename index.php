<?php
require_once(__DIR__ . '/autoload.php');

$url = 'http://nv.fotbal.cz/domaci-souteze/kao/pardubicky/souteze.asp?soutez=530A2A&show=Los';
//$url = 'http://nv.fotbal.cz/domaci-souteze/svitavy/souteze.asp?soutez=533A3A&show=Los'; // invalid URL

$dateAutumn = '';
$dateSpring = '1.4.2014 17:00';
$teams = array(
    'FC Jiskra' => 'Králíky',
    'Staré Hradiště' => 'Staré Hradiště',
    'Č. Třebová B' => 'Česká Třebová B',
    'Prosetín' => 'Prosetín',
    'Načešice' => 'Načešice',
    'M. Třebová' => 'Moravská Třebová',
    'Srch' => 'Srch',
    'Morašice' => 'Morašice',
    'Horní Jelení' => 'Horní Jelení',
    'Slatiňany' => 'Slatiňany',
    'Chrudim C' => 'Chrudim C',
    'Přelouč' => 'Přelouč',
    'Hlinsko B' => 'Hlinsko B',
    'Pomezí' => 'Pomezí'
);

$errors = new \STM\Plugin\ParserFotbalCZ\Errors\ParserErrors();
$parser = new STM\Plugin\ParserFotbalCZ\ParserFotbalCZ();
$parser->setErrorsHandler($errors);
$parser->parse($url);
if ($errors->isValid()) {
    $parser->replaceEmptyDates($dateAutumn, $dateSpring);
} else {
    echo var_dump($errors->getErrors());
    die;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Pomocník pro import rozpisů z fotbal.cz</title>
        <style>
            code {
                display: block;
                border: 1px solid #aaa;
                background: #eee;
                padding: 1em;
            }
        </style>
    </head>
    <body>
        <h1>Pomocník pro import rozpisů z fotbal.cz</h1>

        <ul>
            <li>Adresa: <strong><?php echo "<a href=\"{$url}\">{$url}</a>"; ?></strong></li>
            <li>Náhrada neznámého datumu na podzim: <strong><?php echo $dateAutumn; ?></strong></li>
            <li>Náhrada neznámého datumu na jaře: <strong><?php echo $dateSpring; ?></strong></li>
            <li>Datum stažení rozpisu z fotbal.cz:
                <strong><?php echo strftime("%d.%m.%Y %H:%M:%S", $parser->getDownloadDate()); ?></strong>
            </li>
        </ul>

        <h2>Týmy v rozpisu</h2>
        <code>
            <?php echo implode('<br />', $parser->getTeamsFromSchedule()); ?>
        </code>

        <h2>Formát</h2>
        <code>
            <?php echo $parser->getImportFormat(); ?>
        </code>
        <h2>Původní rozpis</h2>
        <code>
            <?php echo nl2br($parser->getSchedule()); ?>
        </code>

        <h2>Data po nahrazení jmen týmů</h2>        
        <code>
            <?php
            $parser->replaceTeamNames($teams);
            echo nl2br($parser->getSchedule());
            ?>
        </code>

    </body>
</html>
