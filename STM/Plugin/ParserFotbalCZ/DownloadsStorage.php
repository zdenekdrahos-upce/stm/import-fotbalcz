<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\ParserFotbalCZ;

class DownloadsStorage
{
    /** @var string */
    private $directory;
    /** @var STM\Plugin\ParserFotbalCZ\Downloader */
    private $downloader;

    public function __construct()
    {
        $this->directory = __DIR__ . '/downloads/';
        $this->downloader = new Downloader();
    }

    public function getDownloadDate($filename)
    {
        if ($this->existsDownload($filename)) {
            return filectime($this->getFilePath($filename));
        }
        return 0;
    }

    public function getPage($filename, $url)
    {
        if ($this->existsDownload($filename)) {
            return $this->getDownloadedPage($filename);
        } else {
            return $this->downloadPage($filename, $url);
        }
    }

    public function existsDownload($filename)
    {
        return file_exists($this->getFilePath($filename));
    }

    public function getDownloadedPage($filename)
    {
        return file_get_contents($this->getFilePath($filename));
    }

    public function downloadPage($filename, $url)
    {
        $page = $this->downloader->downloadPage($url);
        $this->savePage($filename, $page);
        return $page;
    }

    private function savePage($filename, $page)
    {
        $path = $this->getFilePath($filename);
        return file_put_contents($path, $page);
    }

    private function getFilePath($filename)
    {
        return $this->directory . $filename . '.txt';
    }
}
