<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\ParserFotbalCZ\Helpers;

class Strings
{
    /**
     *
     * @param  string $text       e.g. downloaded page
     * @param  string $startText  sequence which all occurences will be in cutted result
     * @param  string $endingText stop mark for cutting
     * @return string
     */
    public function cutString($text, $startText, $endingText)
    {
        $firstStart = mb_strpos($text, $startText, 0, 'UTF-8');
        $lastStart = mb_strrpos($text, $startText, $firstStart, 'UTF-8');
        $endText = mb_strpos($text, $endingText, $lastStart, 'UTF-8');
        $cutLength = $endText - $firstStart;
        return mb_substr($text, $firstStart, $cutLength, 'UTF-8');
    }

    /**
     * @param  string $text
     * @return string
     */
    public function deleteEmptyLines($text)
    {
        return preg_replace('/^[ \t]*[\r\n]+/m', '', $text);
    }

    /**
     * @param  string $text
     * @return string
     */
    public function cutStartingNumbers($text)
    {
        $matches = array();
        preg_match("/\D/is", $text, $matches, PREG_OFFSET_CAPTURE);
        $firstNonNumericChar = $matches[0][1];
        return substr($text, $firstNonNumericChar);
    }
}
