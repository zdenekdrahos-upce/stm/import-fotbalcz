<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\ParserFotbalCZ\Helpers;

class URL
{

    /**
     * @param  string $url
     * @return array
     */
    public static function getQueryStringArray($url)
    {
        $questionMark = strpos($url, '?');
        if (is_int($questionMark)) {
            $queryString = substr($url, $questionMark + 1);
            $parsedQueryString = array();
            parse_str($queryString, $parsedQueryString);

            return $parsedQueryString;
        } else {
            return array();
        }
    }
}
