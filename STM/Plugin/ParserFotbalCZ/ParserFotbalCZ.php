<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\ParserFotbalCZ;

use STM\Plugin\ParserFotbalCZ\Helpers\URL;
use STM\Plugin\ParserFotbalCZ\Errors\IParserErrors;

class ParserFotbalCZ
{
    /** @var \STM\Plugin\ParserFotbalCZ\Errors\IParserErrors */
    private $errors;
    /** @var STM\Plugin\ParserFotbalCZ\UrlValidator */
    private $urlValidator;
    /** @var \STM\Plugin\ParserFotbalCZ\DownloadsStorage */
    private $storage;
    /** @var \STM\Plugin\ParserFotbalCZ\ScheduleParser */
    private $parser;
    /** @var \STM\Plugin\ParserFotbalCZ\ScheduleFixer */
    private $fixer;
    /** @var \STM\Plugin\ParserFotbalCZ\TeamsFinder */
    private $teamsFinder;
    /** @var string */
    private $competitionId;
    /** @var string */
    private $schedule;

    public function __construct()
    {
        $this->urlValidator = new UrlValidator();
        $this->storage = new DownloadsStorage();
        $this->parser = new ScheduleParser();
        $this->fixer = new ScheduleFixer();
        $this->teamsFinder = new TeamsFinder();
    }

    final public function setErrorsHandler(IParserErrors $errors)
    {
        $this->errors = $errors;
        $this->urlValidator->setErrorsHandler($this->errors);
    }

    /**
     * @param string $url
     */
    public function parse($url)
    {
        $this->urlValidator->validateUrl($url);
        if ($this->errors->isValid()) {
            $this->loadCompetitionId($url);
            $page = $this->storage->getPage($this->competitionId, $url);
            $this->schedule = $this->parser->parseSchedule($this->competitionId, $page);
            if ($this->schedule == '') {
                $this->errors->addInvalidSchedule();
            }
        }
    }

    private function loadCompetitionId($url)
    {
        $queryString = URL::getQueryStringArray($url);
        $this->competitionId = $queryString['soutez'];
    }

    public function replaceEmptyDates($dateAutumn, $dateSpring)
    {
        $this->schedule = $this->fixer->fixDates($this->schedule, $dateAutumn, $dateSpring);
    }

    public function replaceTeamNames(array $teams)
    {
        $this->schedule = $this->fixer->fixTeamNames(
            $this->schedule,
            $this->getTeamsFromSchedule(),
            $teams
        );
    }

    public function getImportFormat()
    {
        return "{R}\t{H}\t{A}\t{D}";
    }

    public function getSchedule()
    {
        return $this->schedule;
    }

    public function getTeamsFromSchedule()
    {
        return $this->teamsFinder->findTeams($this->schedule);
    }

    public function getDownloadDate()
    {
        return $this->storage->getDownloadDate($this->competitionId);
    }
}
