<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\ParserFotbalCZ;

class Downloader
{

    public function downloadPage($url, $connectionTimeout = 5)
    {
        $session = curl_init();
        curl_setopt($session, CURLOPT_URL, $url);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($session, CURLOPT_CONNECTTIMEOUT, $connectionTimeout);
        $page = curl_exec($session);
        curl_close($session);

        return $page;
    }
}
