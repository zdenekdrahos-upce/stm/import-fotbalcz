<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\ParserFotbalCZ;

class TeamsFinder
{

    private $teams = array();

    public function findTeams($schedule)
    {
        $this->teams = array();
        $lines = explode("\n", $schedule);
        $isNotThirdRound = true;
        while ($isNotThirdRound) {
            $line = array_shift($lines);
            $tabArray = explode("\t", $line);
            $this->addTeam($tabArray[1]);
            $this->addTeam($tabArray[2]);
            $isNotThirdRound = $tabArray[0] <= 2;
        }

        return $this->teams;
    }

    private function addTeam($team)
    {
        if (!in_array($team, $this->teams, true)) {
            $this->teams[] = $team;
        }
    }
}
