<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\ParserFotbalCZ\Errors;

class ParserErrors implements IParserErrors
{
    /** @var array */
    private $errors = array();

    public function cleanErrors()
    {
        $this->errors = array();
    }

    /** @return array */
    public function getErrors()
    {
        return $this->errors;
    }

    /** @return boolean */
    public function isValid()
    {
        return empty($this->errors);
    }

    public function addInvalidUrl()
    {
        $this->errors[] = 'Vložte správnou URL adresu';
    }

    public function addInvalidUrlDomain()
    {
        $this->errors[] = 'Adresa musí být z domény http://nv.fotbal.cz/';
    }

    public function addInvalidParameterShow()
    {
        $this->errors[] = 'Na webu fotbal.cz musíte být na stránce Los soutěže (show=Los)';
    }

    public function addInvalidParameterSoutez()
    {
        $this->errors[] = 'Pravděpodobně není vybrána soutěž (soutez=...)';
    }

    public function addInvalidSchedule()
    {
        $this->errors[] = 'Rozpis se nepodařilo načíst, opravdu vidíte na zadané adrese rozlosování soutěže?';
    }
}
