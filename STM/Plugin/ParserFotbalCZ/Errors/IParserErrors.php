<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\ParserFotbalCZ\Errors;

interface IParserErrors
{

    public function isValid();

    public function addInvalidUrl();

    public function addInvalidUrlDomain();

    public function addInvalidParameterShow();

    public function addInvalidParameterSoutez();

    public function addInvalidSchedule();
}
