<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\ParserFotbalCZ;

use STM\Plugin\ParserFotbalCZ\Helpers\Strings;

class ScheduleParser
{
    /** @var string */
    private $schedule;
    /** @var \STM\Plugin\ParserFotbalCZ\Helpers\Strings */
    private $stringHelper;

    public function __construct()
    {
        $this->stringHelper = new Strings();
    }

    /**
     * @param  string $competitionId  competitionId from url parameter 'soutez'
     * @param  string $downloadedPage html page from fotbal.cz
     * @return string
     * Parsed schedule where each line is in format:
     * Round\tHomeTeam\tAwayTeam\tDate
     */
    public function parseSchedule($competitionId, $downloadedPage)
    {
        $this->schedule = $downloadedPage;
        $this->convertToUTF8();
        $this->deletePageLayout();
        $this->replaceCellsWithTabulators();
        $this->stripHtmlTags();
        $this->deleteEmptyLines();
        $this->filterOnlyMatchInfo();
        $this->replaceCompetitionCodeInRounds($competitionId);

        return $this->schedule;
    }

    private function convertToUTF8()
    {
        $this->schedule = iconv('iso-8859-2', 'UTF-8', $this->schedule);
    }

    private function deletePageLayout()
    {
        $this->schedule = $this->stringHelper->cutString(
            $this->schedule,
            '<tr bgcolor="bbbbbb">',
            '</table>'
        );
    }

    private function replaceCellsWithTabulators()
    {
        $this->schedule = str_replace('</td><td>', "\t", $this->schedule);
    }

    private function stripHtmlTags()
    {
        $this->schedule = strip_tags($this->schedule);
    }

    private function deleteEmptyLines()
    {
        $this->page = $this->stringHelper->deleteEmptyLines($this->schedule);
    }

    private function filterOnlyMatchInfo()
    {
        $newLines = array();
        $currentLines = explode("\n", $this->schedule);
        foreach ($currentLines as $line) {
            $cells = explode("\t", $line);
            // at least four cell and 1st cell doesn't contain 'Zápas'
            if (count($cells) > 4 && trim($cells[0]) != 'Zápas') {
                // save only first four cells
                $slice = array_slice($cells, 0, 4);
                // delete match code in first cell
                $slice[0] = substr($slice[0], 0, -2);
                $newLines[] = implode("\t", $slice);
            }
        }
        $this->schedule = implode("\n", $newLines);
    }

    private function replaceCompetitionCodeInRounds($competitionId)
    {
        $shortenId = $this->stringHelper->cutStartingNumbers($competitionId);
        $this->schedule = str_replace(array("{$shortenId}0", $shortenId), '', $this->schedule);
    }
}
