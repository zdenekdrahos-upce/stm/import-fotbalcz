<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\ParserFotbalCZ;

class ScheduleFixer
{
    /** @var int */
    private $actualYear;
    /** @var string */
    private $schedule;

    public function __construct()
    {
        $this->actualYear = strftime("%Y");
    }

    public function fixTeamNames($schedule, array $oldTeams, array $teams)
    {
        return str_replace(array_values($oldTeams), array_values($teams), $schedule);
    }

    public function fixDates($schedule, $dateAutumn, $dateSpring)
    {
        $this->schedule = $schedule;
        $this->addYearToDates();
        $schedules = $this->splitIntoYears();
        $newAutumn = $this->replaceEmptyDate($schedules['autumn'], $dateAutumn);
        $newSpring = $this->replaceEmptyDate($schedules['spring'], $dateSpring);

        return $newAutumn . $newSpring;
    }

    private function addYearToDates()
    {
        $dates = $this->appendYearToMonths();
        $this->schedule = str_replace(array_keys($dates), array_values($dates), $this->schedule);
    }

    private function appendYearToMonths()
    {
        $dates = array();
        for ($i = 1; $i <= 12; $i++) {
            $year = $i >= 7 ? $this->actualYear : ($this->actualYear + 1);
            if ($i < 10) {
                $search = ".0{$i}.";
            } else {
                $search = ".{$i}.";
            }
            $dates[$search] = "{$search}{$year}";
        }

        return $dates;
    }

    private function splitIntoYears()
    {
        $scheduleLength = mb_strlen($this->schedule, 'UTF-8');
        $lastActualYear = mb_strrpos($this->schedule, $this->actualYear, 0, 'UTF-8');
        $springLength = $scheduleLength - $lastActualYear;

        return array(
            'autumn' => mb_substr($this->schedule, 0, $lastActualYear, 'UTF-8'),
            'spring' => mb_substr($this->schedule, $lastActualYear, $springLength, 'UTF-8')
        );
    }

    private function replaceEmptyDate($text, $newDate)
    {
        return str_replace("  .  . 00:00", $newDate, $text);
    }
}
