<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\ParserFotbalCZ;

use STM\Plugin\ParserFotbalCZ\Helpers\URL;
use STM\Plugin\ParserFotbalCZ\Errors\IParserErrors;

class UrlValidator
{
    /** @var ParserErrors */
    private $errors;
    /** @var string */
    private $url;

    public function setErrorsHandler(IParserErrors $errors)
    {
        $this->errors = $errors;
    }

    /** @param string $url */
    public function validateUrl($url)
    {
        $this->url = $url;
        $this->checkErrorsHandler();
        $this->checkUrlAddress();
        if ($this->errors->isValid()) {
            $this->checkUrlDomain();
            $this->checkUrlParameters();
        }
    }

    private function checkErrorsHandler()
    {
        if (is_null($this->errors)) {
            throw new \BadMethodCallException();
        }
    }

    private function checkUrlAddress()
    {
        if (filter_var($this->url, FILTER_VALIDATE_URL) === false) {
            $this->errors->addInvalidUrl();
        }
    }

    private function checkUrlDomain()
    {
        $domain = substr($this->url, 0, 20);
        if ($domain != 'http://nv.fotbal.cz/') {
            $this->errors->addInvalidUrlDomain();
        }
    }

    private function checkUrlParameters()
    {
        $queryStringArray = URL::getQueryStringArray($this->url);
        if (!isset($queryStringArray['show']) || $queryStringArray['show'] != 'Los') {
            $this->errors->addInvalidParameterShow();
        }
        if (!isset($queryStringArray['soutez']) || strlen($queryStringArray['soutez']) < 3) {
            $this->errors->addInvalidParameterShow();
        }
    }
}
